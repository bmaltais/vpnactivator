//
//  VPNActivatorAppDelegate.m
//  VPNActivator
//
//  Created by Bernard Maltais on 11-02-06.
//  Copyright 2011 Bank of Canada. All rights reserved.
//

#import "VPNActivatorAppDelegate.h"

@implementation VPNActivatorAppDelegate

@synthesize window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
}

@end
