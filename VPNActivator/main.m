//
//  main.m
//  VPNActivator
//
//  Created by Bernard Maltais on 11-02-06.
//  Copyright 2011 Bank of Canada. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
