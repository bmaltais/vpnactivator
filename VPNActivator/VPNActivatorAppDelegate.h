//
//  VPNActivatorAppDelegate.h
//  VPNActivator
//
//  Created by Bernard Maltais on 11-02-06.
//  Copyright 2011 Bank of Canada. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface VPNActivatorAppDelegate : NSObject <NSApplicationDelegate> {
@private
    NSWindow *window;
}

@property (assign) IBOutlet NSWindow *window;

@end
